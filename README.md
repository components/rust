# Rust CI/CD Component

> 🚧 **NOTE** 🚧
>
> This component is [being migrated from CI/CD templates](https://gitlab.com/gitlab-org/gitlab/-/issues/437104) and **work-in-progress**, where inputs, template names, and interfaces might change through 0.x.y releases.
>
> Please wait for 1.x.y releases for production usage.

<!-- Briefly describe the capabilities offered by the component project. -->

This component provides job templates to build, test and run Rust source code.

## Build

The `build` component runs `cargo build`.

```yaml
include:
  - component: gitlab.com/components/rust/build@<VERSION>
    inputs:
      job_name: build
      rust_image: rust:latest
      stage: build
```

where `<VERSION>` is the latest released tag, `main` or `~latest`.

### Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `job_name` | `build` | The job name |
| `stage` | `build` | The stage name |
| `rust_image` | `rust:latest` | The Rust image for CI/CD job |

## Test

The `test` component runs `cargo test`.

```yaml
include:
  - component: gitlab.com/components/rust/test@<VERSION>
    inputs:
      job_name: test
      rust_image: rust:latest
      stage: test
```

where `<VERSION>` is the latest released tag, `main` or `~latest`.

### Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `job_name` | `test` | The job name |
| `stage` | `test` | The test stage name |
| `rust_image` | `rust:latest` | The Rust image for CI/CD job |

## Doc

The `doc` component runs `cargo doc`.

```yaml
include:
  - component: gitlab.com/components/rust/doc@<VERSION>
    inputs:
      job_name: doc
      rust_image: rust:latest
      stage: doc
```

where `<VERSION>` is the latest released tag, `main` or `~latest`.

### Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `job_name` | `doc` | The job name |
| `stage` | `doc` | The doc stage name |
| `rust_image` | `rust:latest` | The Rust image for CI/CD job |

## Run

The `run` component runs `cargo run`.

```yaml
include:
  - component: gitlab.com/components/rust/run@<VERSION>
    inputs:
      job_name: run
      rust_image: rust:latest
      stage: run
```

where `<VERSION>` is the latest released tag, `main` or `~latest`.

### Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `job_name` | `run` | The job name |
| `stage` | `run` | The run stage name |
| `rust_image` | `rust:latest` | The Rust image for CI/CD job |

## Full pipeline

You can add the full pipeline component to an existing `.gitlab-ci.yml` file by using the `include:`
keyword.

```yaml
include:
  - component: gitlab.com/components/rust/full-pipeline@<VERSION>
```

where `<VERSION>` is the latest released tag, `main` or `~latest`.

The full pipeline requires the following stages defined:

```yaml
stages:
  # Component specific stages
  - build
  - test
  - doc 
  - run 
```

### Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `rust_image` | `rust:latest` | The Rust image for CI/CD job |

## Contributing

<!-- Notes and how to get intouch with the maintainers. Usually the contribution process should follow the official guide in https://docs.gitlab.com/ee/ci/components/index.html -->

Please read about CI/CD components and best practices at: https://docs.gitlab.com/ee/ci/components 

This component includes a working Rust project, with the source code generated with the help of AI-powered [GitLab Duo](https://about.gitlab.com/gitlab-duo/). You can inspect the source code in the [src/](src/) directory. 

The [.gitlab-ci.yml](.gitlab-ci.yml) configuration tests all components against different Rust images, and more inputs.

Author: Michael Friedrich, @dnsmichi