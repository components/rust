// Generate functions that greet the user running the CI/CD component
// Add PI calculations, a long memory exhaustive test function 
// Conclude with a main function, which calls all functions and says that everything was generated using GitLab Duo


fn greet_user() {
    println!("Hello, user!");
}

const PI: f64 = 3.14159265358979323846;

fn calculate_pi_approximation(iterations: u64) -> f64 {
    let mut pi = 0.0;
    let mut sign = 1.0;
    for i in 0..iterations {
        pi += sign / (2.0 * i as f64 + 1.0);
        sign *= -1.0;
    }
    pi * 4.0
}

fn long_memory_test() {
    let mut vec = Vec::with_capacity(1_000_000_000);
    for i in 0..1_000_000_000 {
        vec.push(i);
    }
}

fn main() {
    greet_user();
    println!("Approximation of PI with 1000 iterations: {}", calculate_pi_approximation(1000));
    long_memory_test();
    println!("All code generated using GitLab Duo");
}
